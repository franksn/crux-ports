#define MOD		SUPER		/* modifier for mouse resize and move */

/* INNER + OUTER should not be greater than BORDERWIDTH */
#define BORDERWIDTH	4		/*  full  border width */
#define INNER		3		/*  inner border width */
#define OUTER		1		/*  outer border width */

/* colors */
#define FOCUSCOL	0xa19999
#define UNFOCUSCOL	0x333230
#define OUTERCOL	0x4c4242

/* resize and move by mouse? */
#define ENABLE_MOUSE
/* sloppy focus? */
#define ENABLE_SLOPPY
/* and double borders? */
#define DOUBLE_BORDER
